# Projet "Créer un moteur de recherche"

## Objectif du projet
Créer un moteur de recherche simpliste (mais le plus abouti possible)

## Fonctionnement général des moteurs de recherche
Un moteur de recherche est composé d'un programme qui explore le web (crawler), d'un algo de tri en fonction de mots clés et en général d'une page web pour afficher les résultats. Il est également composé de données stockées qui peuvent être générées par l'exploration du web réalisée par le crawler OU par d'autres sources de données, notamment l'historique des recherches précédentes (ou via des data broker).

### Le crawler
Le crawler a pour rôle d'explorer le web et de l'indexer. L'opération d'indexation consiste a enregistré les données associée au page de façon à pouvoir les exploiter rapidement par la suite. Le mode d'exploration est assez simple: on nourri le crawler avec des pages qui servent de points d'entrée, puis on parcours ces pages pour trouver les liens, on peut alors ajouter ces liens aux pages à explorer et ainsi de suite. Les aspirateur de site web ont un fonctionnement similaire sauf que le crawler ne s'intéresse pas toujours à l'ensemble des liens du site (par exemple on peut décider d'exclure les script JavaScript ou CSS).

Les crawler de Google ou de Qwant exploite des milliers de machine et demande beaucoup de stockage. Qwant a besoin de plusieurs centaine de salariés et il n'héberge pas eux même toutes les machines qu'ils exploitent. Dans notre cas, nous utiliserons un simple ordinateur sans puissance, sans bande passante, donc les données générées seront limitées. Il faudra donc limiter les recherches à une thématique.

### Le tri
L'ordonnancement des résultats est crucial dans un moteur de recherche. L'algo d'ordonnancement de google ne cessent d'être étudié pour trouver des astuces pour être en numéro 1. En général, le triptyque "mot clé présent dans l'url ou le domaine" ET "mot clé présent dans le chapeau" ET "mot clé présent dans le titre" est assez efficace pour se positionner. Google ou qwant prennent également en compte les liens à destination du site (ainsi que le texte du lien).

Le tri prend également en compte la "distance" entre le mot clé et le mot éffectivement trouvé. Exemple d'algo de calcul de distance: https://fr.wikipedia.org/wiki/Distance_de_Levenshtein

### Les données
Ce tri ne peut s'opérer correctement que si l'index ou les index sont efficaces et si l'architecture pour chercher dans l'index l'est aussi. Plus il y aura de données plus il faut de puissance et de complexité, typiquement l'index sera répartis sur plusieurs machines, etc...

Il faut aussi stocker l'historique des recherches, ce qui permet également d'affiner les résultats les plus demandés, quitte à employer des personnes pour compléter/corriger le tri humainement... D'autres données sur l'utilisateur faisant la recherche peuvent s'avérer utile (graphe social etc...). A noter que cette personnalisation induit le phénomène de "bulle de filtre" et aussi de chambre d'écho.

### La page web
C'est le plus simple (enfin tant qu'on ne reçoit pas 1 000 000 de demande à la seconde), mais dans votre cas ça nécessite de faire tourner un serveur web minimaliste en python + apprendre le HTML.

## Ce qu'il est demandé
Évidemment nous n'avons pas le temps de créer un vrai moteur de recherche, c'est le travail de dizaine d'années de travail pour des centaines de personnes (cf Qwant).
L'objectif est simple faite du mieux que vous pouvez pour que vos résultats ressemblent à quelques choses.

### Délivrables attendus

    un crawler

    un programme de tri en fonction de mots clés

    un code sous git, transmis sous forme de Merge Request pour que je puisse faire des review de votre code (pour que vous puissiez progresser)

    (optionnel) une page web de recherche


### Contraintes

    Pas de librairies comme Scrappy (imaginez plutôt que vous écrivez une alternative à Scrappy)



## Pour démarrer
Pour démarrer je vous conseille de faire un simple dictionnaire stocké dans un fichier, par exemple en json. Ce fichier associera le titre de la page à son URL. Votre crawler va donc au fur et à mesure de son exploration enregistrer ces données.

ATTENTION: ne soyez pas trop brute avec les sites web... Peut être faut il mettre une tempo dans certains cas, et vérifier que vous ne tourner pas en boucle... (pour la tempo cf la doc de time.sleep(secs) )

Puis créer un petit input pour taper un mot et le chercher dans votre dictionnaire

Ensuite il existe plein de façon d'améliorer tout ça, le texte au dessus décrivant le fonctionnement général ainsi que des recherches sur le fonctionnement des moteurs de recherche permet de trouver des idées pour améliorer tout ça.

## A propos de l'index

La forme de l'index peut prendre diverse forme. Dans un livre, le but d'un index c'est de retrouver rapidement une page. En informatique c'est la même chose, l'index permet de retrouver rapidement un élément (qui peut être une page web). L'index peut être constitué avec chaque mot (ou lemme*) rencontré suivi des pages où il a été rencontré
```
bricol https://www.mr-bricolage.fr/ [...]
bois https://fr.wikipedia.org/wiki/Bois https://www.manomano.fr/bois-3959
cuisine https://marmiton.org https://www.ikea.com/fr/fr/rooms/kitchen/ [...]
```
lemme = racine d'un mot

Les petits mots sont en général exclus

## A propos de la centralisation
Compte tenu du fait qu'il y a de nombreux enjeux en terme de quantité de données, de régulation des sites web qui essaient de se placer en premier, de confiance dans les données générées, de réactivité etc... Il semble assez complexe d'envisager un moteur de recherche décentralisé. Il y a bien Yacy, mais le projet a vraiment du mal à décoller. D'autres services sont plus simple à décentraliser.
